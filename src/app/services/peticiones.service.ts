import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class PeticionesService {

  constructor(
    private _http : HttpClient
  ) { }

  public getAllRecursos(){
    return this._http.get<any>('https://rickandmortyapi.com/api/character');
  }
}
