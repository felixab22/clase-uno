import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';
import { PagesComponent } from './pages/pages.component';
import { BienvenidoComponent } from './pages/bienvenido/bienvenido.component';
import { InicioComponent } from './pages/inicio/inicio.component';
 
const routes: Routes = [
  {
    path:'',
    component:LoginComponent
  },
  {
    path:'UNSCH',
    component:PagesComponent,
    children:
    [
      {
        path:'Bienvenido',
        component:BienvenidoComponent
      },
      {
        path:'Inicio',
        component:InicioComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
