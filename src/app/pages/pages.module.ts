import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { GeneroPipe } from '../pipes/genero.pipe';
import { SharedModule } from '../shared/shared.module';

import { AppRoutingModule } from '../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { InicioComponent } from './inicio/inicio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PagesComponent,
    BienvenidoComponent,
    GeneroPipe,
    InicioComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    SharedModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class PagesModule { }
