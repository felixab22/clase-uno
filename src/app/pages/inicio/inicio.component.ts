import { Component, OnInit } from '@angular/core';
import { PersonaModel } from 'src/app/model/peticiones.modal';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  mostar = false;
  lista = ['FELIX','PEDRO', 'JUAN', "RAUL"]; 
  name = '';
  nuevaPersona : PersonaModel
  constructor() {
    this.nuevaPersona = new PersonaModel();

   }

  ngOnInit(): void {
  }
  operacion(){
    this.mostar =! this.mostar;
  }
  enviarmodel(){
    console.log(this.nuevaPersona);
    
  }
}
