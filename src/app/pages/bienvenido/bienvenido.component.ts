import { Component, OnInit } from '@angular/core';
import { PetecionesModal } from 'src/app/model/peticiones.modal';
import { PeticionesService } from 'src/app/services/peticiones.service';

@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {
  title = 'primero';
  listarPeticion: Array<PetecionesModal>;
  unopersonaje: PetecionesModal
  
  
  constructor(
    private _peticionSrv : PeticionesService
    ){
      this.unopersonaje = new PetecionesModal();
      this.listarPeticion = new Array<PetecionesModal>();

  }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this._peticionSrv.getAllRecursos().subscribe(res => {
      this.listarPeticion = res.results
      // console.log(this.listarPeticion);
    })
  }

  

  verMas(valor){
    this.unopersonaje = valor;
    console.log(this.unopersonaje);
  }

}
