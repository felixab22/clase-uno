import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  validateForm: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.validateForm = new FormGroup(
      {
        dni: new FormControl('', [Validators.required, Validators.maxLength(8)]),
        codigo: new FormControl('', [Validators.required, Validators.minLength(8)])
      }
    )
  }
  enviar(valor){
    console.log(valor);
      this._router.navigate(['/UNSCH/Bienvenido']);
  }
}
